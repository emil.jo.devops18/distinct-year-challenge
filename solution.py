def distinct(year):
    new_year = year+1
    if len(set(str(new_year))) == 4:
        return new_year
    while len(set(str(new_year))) < 4:
        new_year += 1
        if len(set(str(new_year))) == 4:
            break
    return new_year
